/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/app.js":
/***/ (function(module, exports) {

$(document).on('click', '.navbar-collapse.in', function (e) {
	if ($(e.target).is('a')) {
		$(this).collapse('hide');
	}
});

$(document).popover({
	selector: '[data-toggle="popover"]'
});

$(document).on('click', function (e) {
	$('[data-toggle="popover"]').each(function () {
		if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
			$(this).popover('hide');
		}
	});
});

$(window).on('resize load', function () {
	if ($(window).width() > 750) $('.chosen-tp-filter').next().css('width', '250px');else $('.chosen-tp-filter').next().css('width', '100%');

	$('.chosen-select-filters').next().css('width', '100%');
});

angular.module("app", ['ui.router', 'angular-loading-bar', 'ghiscoding.validation', 'pascalprecht.translate', 'satellizer', 'infinite-scroll', 'angular.chosen', 'toaster', 'ngStorage', 'ngSanitize']).filter('conax', function () {
	return function (input) {
		return input.replace('Conax', '');
	};
}).constant('API_URL', 'http://search.loc').service('View', ['$rootScope', function ($rootScope) {
	return {
		title: function title(_title) {
			$rootScope.title = _title;
		}
	};
}]).run(['$rootScope', 'Auth', '$state', function ($rootScope, Auth, $state) {
	$rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
		if (Auth.isAuth() && (toState.name === 'app.login' || toState.name === 'app.register')) {
			event.preventDefault();
			$state.transitionTo('app.index');
		}

		if (toState.authenticate && !Auth.isAuth()) {
			event.preventDefault();
			$state.transitionTo('app.login');
		}
	});
}]).factory('TokenIntecerptor', ['$q', '$location', 'toaster', '$localStorage', '$rootScope', function ($q, $location, toaster, $localStorage, $rootScope) {
	return {
		'responseError': function responseError(rejection) {
			var rejectionReasons = ['token_not_provided', 'token_expired', 'token_absent', 'token_invalid'];

			angular.forEach(rejectionReasons, function (value, index) {
				if (rejection.data.error === value || rejection.status === 400) {
					delete $rootScope.auth;
					delete $rootScope.user;
					delete $localStorage.auth;
					delete $localStorage.user;

					toaster.pop({
						type: 'error',
						title: '¡Error!',
						body: 'Vuelve a iniciar sesión'
					});

					$location.path('/');
				}
			});

			return $q.reject(rejection);
		}
	};
}]).directive('autofocus', ['$timeout', function ($timeout) {
	return {
		restrict: 'A',
		link: function link($scope, $element) {
			$timeout(function () {
				$element[0].focus();
			});
		}
	};
}]).directive('loading', function () {
	return {
		restrict: 'E',
		template: '\n\t        <h3 class="text-center">\n\t            <i class="fa fa-circle-o-notch fa-spin fa-5x fa-fw"></i>\n\t        </h3>\n\t\t',
		scope: {
			check: "&"
		},
		link: function link(scope, elm, attrs) {
			scope.$watch(scope.check, function (v) {
				if (v) {
					elm.show();
				} else {
					elm.hide();
				}
			});
		}
	};
});

/***/ }),

/***/ "./app/config.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').config(config);

    config.$inject = ['$stateProvider', '$translateProvider', '$urlRouterProvider', '$authProvider', 'API_URL', '$locationProvider', '$httpProvider'];

    function config($stateProvider, $translateProvider, $urlRouterProvider, $authProvider, API_URL, $locationProvider, $httpProvider) {

        $authProvider.loginUrl = API_URL + '/authenticate';
        $httpProvider.interceptors.push('TokenIntecerptor');
        $httpProvider.interceptors.push(['$q', '$rootScope', function ($q, $rootScope) {
            var numberOfHttpRequests = 0;
            return {
                request: function request(config) {
                    numberOfHttpRequests += 1;
                    $rootScope.waiting = true;
                    return config;
                },
                requestError: function requestError(error) {
                    numberOfHttpRequests -= 1;
                    $rootScope.waiting = numberOfHttpRequests !== 0;
                    return $q.reject(error);
                },
                response: function response(_response) {
                    numberOfHttpRequests -= 1;
                    $rootScope.waiting = numberOfHttpRequests !== 0;
                    return _response;
                },
                responseError: function responseError(error) {
                    numberOfHttpRequests -= 1;
                    $rootScope.waiting = numberOfHttpRequests !== 0;
                    return $q.reject(error);
                }
            };
        }]);

        $translateProvider.useStaticFilesLoader({
            files: [{
                prefix: 'js/validation/',
                suffix: '.json'
            }]
        });

        $translateProvider.preferredLanguage('es');
        $translateProvider.useSanitizeValueStrategy('escapeParameters');

        $stateProvider.state('app', {
            abstract: true,
            templateUrl: 'app/templates/partials/nav.html',
            controller: 'NavCtrl as main'
        }).state('app.index', {
            url: '/',
            templateUrl: 'app/templates/main.html',
            controller: 'HomeCtrl as home'
        }).state('app.transponders', {
            url: '/transponders/:satelite',
            templateUrl: 'app/templates/transponders.html',
            controller: 'TpCtrl as tp'
        }).state('app.states', {
            url: '/states',
            templateUrl: 'app/templates/states.html',
            controller: 'StateCtrl as state'
        }).state('app.create', {
            url: '/states/create',
            templateUrl: 'app/templates/state-create.html',
            controller: 'StateCreateCtrl as stateCreate'
        }).state('app.comments', {
            url: '/states/:id/comments',
            templateUrl: 'app/templates/comments.html',
            controller: 'CommentCtrl as comment'
        }).state('app.notificions', {
            url: '/notifications',
            templateUrl: 'app/templates/notifications.html',
            controller: 'NotificationCtrl as notification'
        }).state('app.register', {
            url: '/register',
            templateUrl: 'app/templates/register.html',
            controller: 'RegisterCtrl as register'
        }).state('app.login', {
            url: '/login',
            templateUrl: 'app/templates/login.html',
            controller: 'LoginCtrl as login'
        }).state('app.edit', {
            url: '/edit',
            templateUrl: 'app/templates/edit.html',
            controller: 'EditCtrl as edit',
            authenticate: true
        }).state('app.history', {
            url: '/history',
            templateUrl: 'app/templates/suscriptions.html',
            controller: 'SuscriptionCtrl as suscription',
            authenticate: true
        });

        $urlRouterProvider.otherwise('/');

        $locationProvider.html5Mode(true).hashPrefix('!');
    }
})();

/***/ }),

/***/ "./app/controllers/CommentCtrl.js":
/***/ (function(module, exports) {

(function () {
	'use strict';

	angular.module('app').controller('CommentCtrl', CommentCtrl);

	CommentCtrl.$inject = ['$http', 'CommentService', '$location', '$stateParams', 'ValidationService', '$scope', 'View', 'toaster'];

	function CommentCtrl($http, CommentService, $location, $stateParams, ValidationService, $scope, View, toaster) {

		View.title("Detalle estado decodificador");

		var vm = this;

		vm.id = $stateParams.id;
		vm.comments = [];
		vm.form = {};
		vm.exists = true;

		CommentService.comments(vm.id).then(function (response) {
			vm.comments = response.data.comments;
			vm.state = response.data.state;
			vm.exists = response.data.exists;
		}, function () {
			$state.go('states');
		});

		vm.comment = function () {
			CommentService.addComment(vm.id, vm.form).then(function (response) {
				var comment = { body: response.data.comment, created_human_at: response.data.created_human_at };
				vm.comments.unshift(comment);
				vm.form = {};
				new ValidationService().resetForm($scope.formComment);
			});
		};

		vm.suscribe = function () {
			CommentService.suscribe(vm.id).then(function (response) {
				vm.exists = true;
				toaster.pop({ type: 'success', title: '¡OK!', body: 'Suscrito correctamente.' });
			});
		};
	}
})();

/***/ }),

/***/ "./app/controllers/EditCtrl.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', EditCtrl);

    EditCtrl.$inject = ['$state', 'API_URL', 'RegisterService', 'Auth', 'View', 'StateService', '$rootScope', 'toaster'];

    function EditCtrl($state, API_URL, RegisterService, Auth, View, StateService, $rootScope, toaster) {

        View.title('Editar cuenta');

        var vm = this;
        vm.form = {
            country_id: $rootScope.user.country_id,
            name: $rootScope.user.name,
            email: $rootScope.user.email,
            username: $rootScope.user.username,
            password: ''
        };

        vm.input = 'password';
        vm.countries = StateService.getCountries();

        vm.hideShowPassword = function () {
            if (vm.input == 'password') vm.input = 'text';else vm.input = 'password';
        };

        vm.edit = function () {
            RegisterService.edit(vm.form).then(function (response) {
                Auth.setUser(response.data.user);
                $state.transitionTo($state.current, {}, { reload: true });
                toaster.pop({ type: 'success', title: 'OK!', body: 'Cuenta editada' });
            });
        };
    }
})();

/***/ }),

/***/ "./app/controllers/HomeController.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('HomeCtrl', HomeCtrl);

    HomeCtrl.$inject = ['API_URL', '$http', 'ChannelsFactory', 'View'];

    function HomeCtrl(API_URL, $http, ChannelsFactory, View) {

        View.title('Canales y TPs de Amazonas, Star One C2 y Arsat 2');

        var vm = this;

        vm.satelite = 2;
        vm.satelites = [{ 'id': 2, 'name': '61W Amazonas' }, { 'id': 4, 'name': '70W Star One' }, { 'id': 1, 'name': '81W Arsat 2' }];

        vm.loader = true;
        vm.transponders = [];
        vm.channels = [];

        ChannelsFactory.get().then(function (response) {
            vm.channels = response.data.channels;
            vm.transponders = response.data.transponders;
            vm.loader = false;
        }, function (error) {
            console.log(error);
        });

        vm.search = function (transponder, satelite) {
            ChannelsFactory.get(transponder, satelite).then(function (response) {
                vm.channels = response.data.channels;
                vm.transponders = response.data.transponders;
                vm.loader = false;
            }, function (error) {
                console.log(error);
            });
        };

        vm.clearTp = function () {

            vm.channels = [];
            vm.satelite = 2;
            vm.transponder = '';
            vm.input = '';
            vm.loader = true;

            ChannelsFactory.get().then(function (response) {
                vm.channels = response.data.channels;
                vm.transponders = response.data.transponders;
                vm.loader = false;
            });
        };

        vm.clearInput = function () {
            vm.input = '';
        };
    }
})();

/***/ }),

/***/ "./app/controllers/LoginController.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('LoginCtrl', LoginCtrl);

    LoginCtrl.$inject = ['$state', 'Auth', 'View', 'toaster'];

    function LoginCtrl($state, Auth, View, toaster) {

        View.title('Iniciar sesión');

        var vm = this;
        vm.credentials = {};

        vm.login = function () {
            Auth.login(vm.credentials).then(function (response) {
                Auth.setUser(response.data.user);
                Auth.setAuth();
                $state.transitionTo('app.index', {}, { reload: true });
            }, function () {
                toaster.pop({ type: 'error', title: '¡Error!', body: 'Datos incorrectos' });
            });
        };
    }
})();

/***/ }),

/***/ "./app/controllers/NavCtrl.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('NavCtrl', NavCtrl);

    NavCtrl.$inject = ['$localStorage', 'Auth', '$state', '$rootScope'];

    function NavCtrl($localStorage, Auth, $state, $rootScope) {

        var vm = this;

        $rootScope.auth = Auth.getAuth();
        $rootScope.user = Auth.getUser();

        vm.logout = function () {
            Auth.logout();
            $state.transitionTo('app.index', {}, { reload: true });
        };
    }
})();

/***/ }),

/***/ "./app/controllers/NotificationCtrl.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('NotificationCtrl', NotificationCtrl);

    NotificationCtrl.$inject = ['API_URL', '$http', 'ValidationService', 'NotificationService', 'View', '$rootScope', 'toaster'];

    function NotificationCtrl(API_URL, $http, ValidationService, NotificationService, View, $rootScope, toaster) {

        View.title('Suscribete');

        var vm = this;
        vm.models = [];
        vm.form = {
            type_id: []
        };

        if ($rootScope.auth) {
            NotificationService.getModels().then(function (response) {
                vm.models = response.data.models;
                vm.form.type_id = response.data.selected;
            });
        }

        vm.notifyModels = function () {
            NotificationService.notifyModels(vm.form).then(function (response) {
                toaster.pop({ type: 'success', title: '¡OK!', body: 'Modelos guardados' });
            });
        };
    }
})();

/***/ }),

/***/ "./app/controllers/RegisterCtrl.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('RegisterCtrl', RegisterCtrl);

    RegisterCtrl.$inject = ['$state', 'API_URL', 'Auth', 'View', 'RegisterService', 'StateService', 'toaster'];

    function RegisterCtrl($state, API_URL, Auth, View, RegisterService, StateService, toaster) {

        View.title('Crear tu cuenta');

        var vm = this;
        vm.form = {};
        vm.input = 'password';
        vm.countries = StateService.getCountries();

        vm.hideShowPassword = function () {
            if (vm.input == 'password') vm.input = 'text';else vm.input = 'password';
        };

        StateService.getCountry().then(function (response) {
            response.data[1] !== undefined ? vm.form.country_id = response.data[1].id : vm.form.country_id = vm.countries[0].id;
        });

        vm.register = function () {
            RegisterService.register(vm.form).then(function (response) {

                Auth.setUser(response.data.user);
                Auth.setAuth();
                toaster.pop({ type: 'success', title: '¡OK!', body: 'Cuenta creada' });
                localStorage.satellizer_token = response.data.token;
                $state.transitionTo('app.index', {}, { reload: true });
            }).catch(function (response) {
                var errors = [];

                angular.forEach(response.data.errors, function (value, key) {
                    errors += value[0] + '<br>';
                });

                toaster.pop({
                    type: 'error',
                    title: '¡Error!',
                    bodyOutputType: 'trustedHtml',
                    body: errors
                });
            });
        };
    }
})();

/***/ }),

/***/ "./app/controllers/StateController.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('StateCtrl', StateCtrl);

    StateCtrl.$inject = ['API_URL', '$http', 'ValidationService', 'StateService', 'View'];

    function StateCtrl(API_URL, $http, ValidationService, StateService, View) {

        View.title('Estado decodificadores');

        var vm = this;

        vm.marks = StateService.getMarks();
        vm.models = [];
        vm.form = {};
        vm.page = 1;
        vm.states = [];
        vm.show = false;
        vm.reset = false;

        vm.nextPage = function () {

            vm.pause = true;
            vm.loader = true;

            StateService.states(vm.page++, vm.form.mark_id, vm.form.model_id, vm.form.method, vm.form.state).then(function (response) {

                var states = response.data.states.data;

                if (response.data.states.to === response.data.states.total) {
                    vm.states = vm.states.concat(states);
                    vm.pause = true;
                } else {
                    vm.pause = false;
                    vm.states = vm.states.concat(states);
                    vm.total = response.data.states.total;
                }

                vm.loader = false;
                vm.reset = false;
            });
        };

        vm.filter = function (mark) {
            StateService.marks(mark).then(function (response) {
                vm.models = response.data.models;
            });
        };

        vm.formFilter = function () {
            StateService.states(1, vm.form.mark_id, vm.form.model_id, vm.form.method, vm.form.state).then(function (response) {

                vm.total = response.data.states.total;
                vm.page = 2;

                if (response.data.states.data.length) {

                    vm.show = false;
                    vm.states = response.data.states.data;

                    if (response.data.states.to === vm.total) vm.pause = true;else vm.pause = false;
                    vm.loader = false;
                } else {
                    vm.show = true;
                    vm.states = [];
                }
            });
        };

        vm.clean = function () {
            vm.loader = true;
            vm.form = {};
            vm.page = 1;
            vm.pause = false;
            vm.show = false;
            vm.reset = true;
            StateService.states(vm.page++).then(function (response) {
                vm.states = response.data.states.data;
                vm.total = response.data.states.total;
                vm.loader = false;
            });
        };
    }
})();

/***/ }),

/***/ "./app/controllers/StateCreateCtrl.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('StateCreateCtrl', StateCreateCtrl);

    StateCreateCtrl.$inject = ['API_URL', '$http', 'ValidationService', '$rootScope', 'StateService', '$state', 'View', 'toaster'];

    function StateCreateCtrl(API_URL, $http, ValidationService, $rootScope, StateService, $state, View, toaster) {

        View.title('Ingresar nuevo estado');

        var vm = this;

        vm.form = {};
        vm.models = [];
        vm.countries = StateService.getCountries();

        vm.marks = [{ "id": 2, "name": "Azamerica" }, { "id": 9, "name": "AzFox" }, { "id": 8, "name": "Cinebox" }, { "id": 7, "name": "Freei" }, { "id": 3, "name": "Freesky" }, { "id": 1, "name": "Miuibox" }, { "id": 6, "name": "QMDA" }, { "id": 5, "name": "Tocomfree" }, { "id": 4, "name": "TocomSat" }];

        vm.satelites = [{ "id": 9, "name": "22W Ses 4" }, { "id": 3, "name": "30W Hispasat" }, { "id": 5, "name": "58W Intelsat" }, { "id": 2, "name": "61W Amazonas" }, { "id": 8, "name": "63W Telstar 14R" }, { "id": 4, "name": "70W Star One" }, { "id": 7, "name": "71.8W Arsat 1" }, { "id": 1, "name": "81W Arsat 2" }, { "id": 6, "name": "87.2W TKSAT" }, { "id": 10, "name": "107.3W Anik G" }];

        if ($rootScope.auth) vm.form.country_id = $rootScope.user.country_id;else {
            StateService.getCountry().then(function (response) {
                response.data[1] !== undefined ? vm.form.country_id = response.data[1].id : vm.form.country_id = vm.countries[0].id;
            });
        }

        vm.filter = function (mark) {
            StateService.marks(mark).then(function (response) {
                vm.models = response.data.models;
                vm.form.model_id = vm.models[0].id;
            });
        };

        vm.send = function () {
            StateService.newState(vm.form).then(function () {
                toaster.pop({ type: 'success', title: '¡OK!', body: 'Estado enviado' });
                $state.transitionTo('app.states');
            }, function (error) {
                alert(error.data.message);
            });
        };
    }
})();

/***/ }),

/***/ "./app/controllers/SuscriptionCtrl.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('SuscriptionCtrl', SuscriptionCtrl);

    SuscriptionCtrl.$inject = ['RegisterService', 'View', 'toaster', '$window'];

    function SuscriptionCtrl(RegisterService, View, toaster, $window) {

        View.title('Actividad de tu cuenta');

        var vm = this;

        vm.suscriptions = [];
        vm.states = [];
        vm.comments = [];
        vm.index = '';
        vm.data = {};

        RegisterService.suscriptions().then(function (response) {
            vm.suscriptions = response.data.suscriptions;
            vm.states = response.data.states;
            vm.comments = response.data.comments;
        });

        vm.delete = function () {
            vm.index = vm.data.id;
            RegisterService.deleteState(vm.data.id).then(function (response) {
                $('#modal-delete').modal('hide');
                toaster.pop({ type: 'success',
                    title: '¡OK!',
                    body: 'Has eliminado la suscripción'
                });
                vm.suscriptions.splice(vm.data.index, 1);
            });
        };

        vm.modal = function (index, id) {
            vm.data.index = index;
            vm.data.id = id;
        };
    }
})();

/***/ }),

/***/ "./app/controllers/TpController.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').controller('TpCtrl', TpCtrl);

    TpCtrl.$inject = ['$stateParams', 'ChannelsFactory', '$location', 'View'];

    function TpCtrl($stateParams, ChannelsFactory, $location, View) {

        var vm = this;
        var satelite = $stateParams.satelite.replace(/\-/g, ' ');

        View.title('Listado de TPs de Amazonas ' + satelite);

        vm.transponders = {};
        vm.count = '';
        vm.satelite = satelite;
        vm.loader = true;

        ChannelsFactory.getTps(vm.satelite).then(function (response) {
            vm.transponders = response.data.transponders;
            vm.count = response.data.count;
            vm.loader = false;
        }, function (error) {
            $location.path('/');
        });
    }
})();

/***/ }),

/***/ "./app/directives/bootstrap.js":
/***/ (function(module, exports) {

(function () {

  var bootstrapValidationDecorator = function bootstrapValidationDecorator() {
    return {
      scope: {
        bootstrapValidationDecorator: '@'
      },
      restrict: 'A',
      require: '^form',
      link: function link(scope, el, attrs, formCtrl) {
        scope.form = formCtrl;

        if (scope.bootstrapValidationDecorator != undefined && scope.bootstrapValidationDecorator != "") {
          scope.fieldName = scope.bootstrapValidationDecorator;
        } else {
          scope.fieldName = angular.element(el[0].querySelector("[name]")).attr('name');
        }

        scope.$watch(function (scope) {
          return scope.form[scope.fieldName].$touched && scope.form[scope.fieldName].$invalid;
        }, function (newVal, oldVal) {
          if (newVal != oldVal) {
            el.toggleClass('has-error', newVal);
          }
        });
      }
    };
  };
  angular.module('app').directive('bootstrapValidationDecorator', bootstrapValidationDecorator);
})();

/***/ }),

/***/ "./app/services/AuthService.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').factory('Auth', Auth);

    Auth.$inject = ['$auth', '$q', '$rootScope', '$location', '$localStorage'];

    function Auth($auth, $q, $rootScope, $location, $localStorage) {

        return {
            login: login,
            isAuth: isAuth,
            setAuth: setAuth,
            getAuth: getAuth,
            setUser: setUser,
            getUser: getUser,
            logout: logout
        };

        function login(data) {

            var deferred = $q.defer();

            $auth.login(data).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function isAuth() {
            return this.getAuth() ? true : false;
        }

        function setAuth() {
            $localStorage.auth = true;
        }

        function getAuth() {
            return $localStorage.auth;
        }

        function setUser(user) {
            $localStorage.user = user;
        }

        function getUser() {
            return $localStorage.user;
        }

        function logout() {
            delete $rootScope.auth;
            delete $localStorage.auth;
            delete $rootScope.user;
            delete $localStorage.user;
            $auth.logout();
        }
    }
})();

/***/ }),

/***/ "./app/services/CommentService.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').factory('CommentService', CommentService);

    CommentService.$inject = ['$http', 'API_URL', '$q'];

    function CommentService($http, API_URL, $q) {

        return {
            comments: comments,
            addComment: addComment,
            suscribe: suscribe
        };

        function comments(state) {

            var deferred = $q.defer();

            $http({
                url: API_URL + '/states/' + state + '/comments'
            }).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function addComment(state, form) {

            var deferred = $q.defer();

            $http.post(API_URL + '/states/' + state + '/comments', form).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function suscribe(id) {
            var deferred = $q.defer();

            $http.post(API_URL + '/suscribe/' + id + '/state').then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
})();

/***/ }),

/***/ "./app/services/HomeService.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').factory('ChannelsFactory', ChannelsFactory);

    ChannelsFactory.$inject = ['$http', 'API_URL', '$q'];

    function ChannelsFactory($http, API_URL, $q) {

        return {
            get: get,
            getTps: getTps
        };

        function get(transponder, satelite) {

            var deferred = $q.defer();

            $http({
                url: API_URL + '/channels',
                method: 'GET',
                params: { transponder: transponder, satelite: satelite }
            }).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function getTps(satelite) {

            var deferred = $q.defer();

            $http({
                url: API_URL + '/transponders/' + satelite,
                cache: true
            }).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
})();

/***/ }),

/***/ "./app/services/NotificationService.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').factory('NotificationService', NotificationService);

    NotificationService.$inject = ['$http', 'API_URL', '$q'];

    function NotificationService($http, API_URL, $q) {

        return {
            getModels: getModels,
            notifyModels: notifyModels
        };

        function getModels() {

            var deferred = $q.defer();

            $http.get(API_URL + '/models').then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function notifyModels(form) {

            var deferred = $q.defer();

            $http.post(API_URL + '/users/models', form).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
})();

/***/ }),

/***/ "./app/services/RegisterService.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').factory('RegisterService', RegisterService);

    RegisterService.$inject = ['$http', 'API_URL', '$q'];

    function RegisterService($http, API_URL, $q) {

        return {
            register: register,
            edit: edit,
            suscriptions: suscriptions,
            deleteState: deleteState
        };

        function register(form) {

            var deferred = $q.defer();

            $http.post(API_URL + '/users', form).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function notifyModels(form) {

            var deferred = $q.defer();

            $http.post(API_URL + '/users/models', form).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function edit(form) {
            var deferred = $q.defer();

            $http.put(API_URL + '/users/edit', form).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function suscriptions() {
            var deferred = $q.defer();

            $http.get(API_URL + '/users/suscriptions').then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function deleteState(id) {
            var deferred = $q.defer();

            $http.delete(API_URL + '/users/suscriptions/' + id).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }
    }
})();

/***/ }),

/***/ "./app/services/StateService.js":
/***/ (function(module, exports) {

(function () {
    'use strict';

    angular.module('app').factory('StateService', StateService);

    StateService.$inject = ['$http', 'API_URL', 'ValidationService', '$q'];

    function StateService($http, API_URL, ValidationService, $q) {

        return {
            states: states,
            marks: marks,
            newState: newState,
            getMarks: getMarks,
            getCountry: getCountry,
            getCountries: getCountries
        };

        function states(page, mark_id, model_id, method, state) {

            var deferred = $q.defer();

            $http({
                url: API_URL + '/',
                params: { page: page, mark_id: mark_id, model_id: model_id, method: method, state: state }
            }).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function marks(mark_id) {

            var deferred = $q.defer();

            $http({
                url: API_URL + '/models/' + mark_id
            }).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function newState(form) {

            var deferred = $q.defer();

            $http.post(API_URL + '/states', form).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function getMarks() {
            return [{ "id": 2, "name": "Azamerica" }, { "id": 9, "name": "AzFox" }, { "id": 8, "name": "Cinebox" }, { "id": 7, "name": "Freei" }, { "id": 3, "name": "Freesky" }, { "id": 1, "name": "Miuibox" }, { "id": 6, "name": "QMDA" }, { "id": 5, "name": "Tocomfree" }, { "id": 4, "name": "TocomSat" }];
        }

        function getCountry() {

            var deferred = $q.defer();

            $http({
                url: API_URL + '/states/geo',
                cache: true
            }).then(function (data) {
                deferred.resolve(data);
            }, function (data) {
                deferred.reject(data);
            });

            return deferred.promise;
        }

        function getCountries() {
            return [{ 'id': 1, 'name': "Chile" }, { 'id': 2, 'name': "Argentina" }, { 'id': 6, 'name': "Bolivia" }, { 'id': 10, 'name': "Brasil" }, { 'id': 4, 'name': "Colombia" }, { 'id': 3, 'name': "Ecuador" }, { 'id': 14, 'name': "El Salvador" }, { 'id': 16, 'name': "Estados Unidos" }, { 'id': 13, 'name': "Guatemala" }, { 'id': 11, 'name': "México" }, { 'id': 15, 'name': "Nicaragua" }, { 'id': 12, 'name': "Panamá" }, { 'id': 9, 'name': "Paraguay" }, { 'id': 5, 'name': "Perú" }, { 'id': 8, 'name': "Uruguay" }, { 'id': 7, 'name': "Venezuela" }];
        }
    }
})();

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./app/app.js");
__webpack_require__("./app/config.js");
__webpack_require__("./app/controllers/HomeController.js");
__webpack_require__("./app/controllers/TpController.js");
__webpack_require__("./app/controllers/StateController.js");
__webpack_require__("./app/controllers/NavCtrl.js");
__webpack_require__("./app/controllers/CommentCtrl.js");
__webpack_require__("./app/services/CommentService.js");
__webpack_require__("./app/controllers/StateCreateCtrl.js");
__webpack_require__("./app/controllers/NotificationCtrl.js");
__webpack_require__("./app/controllers/LoginController.js");
__webpack_require__("./app/controllers/RegisterCtrl.js");
__webpack_require__("./app/controllers/EditCtrl.js");
__webpack_require__("./app/controllers/SuscriptionCtrl.js");
__webpack_require__("./app/services/StateService.js");
__webpack_require__("./app/services/HomeService.js");
__webpack_require__("./app/services/AuthService.js");
__webpack_require__("./app/services/NotificationService.js");
__webpack_require__("./app/services/RegisterService.js");
module.exports = __webpack_require__("./app/directives/bootstrap.js");


/***/ })

/******/ });