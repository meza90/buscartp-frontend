<?php

$str = file_get_contents('mix-manifest.json');
$str = json_decode($str, true);

?>
<!DOCTYPE html>
<html lang="es" ng-app="app">
<head>
    <base href="/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Listado actualizado de canales y tps de satélite amazonas, star one c2 y arsat 2">
    <meta name="author" content="buscartp.com">
    <meta name="keywords" content="canales del satélite amazonas star one c2 arsat 2, tps del satélite amazonas star one c2 arsat 2, lista de canales del satélite amazonas star one c2 arsat 2, tps amazonas star one c2 arsat 2 actualizados 2017">
    <meta name="robots" content="index, follow">

    <title ng-bind="title + ' - BuscarTP'">BuscarTP</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/flatly/bootstrap.min.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/angular-loading-bar/0.9.0/loading-bar.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    <link rel="stylesheet" href="<?php echo $str['/css/app.css'] ?>">
    <link rel="icon" href="images/ico.png">
</head>
<body>

    <ui-view></ui-view>
    <toaster-container toaster-options="{'limit': 1, 'time-out': 4000, 'close-button': true}">
    </toaster-container>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.4.3/angular-ui-router.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ngStorage/0.3.11/ngStorage.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.6.1/angular-sanitize.min.js"></script>
    <script src="js/vendor.js"></script>
    <script src="<?php echo $str['/js/app.js']; ?>"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-101174438-1', 'auto');
        ga('send', 'pageview');
    </script>
</body>
</html>
