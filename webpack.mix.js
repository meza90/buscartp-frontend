let mix = require('laravel-mix');

mix.disableNotifications();

mix.js([
		'app/app.js',
		'app/config.js',
		'app/controllers/HomeController.js',
		'app/controllers/TpController.js',
		'app/controllers/StateController.js',
		'app/controllers/NavCtrl.js',
		'app/controllers/CommentCtrl.js',
		'app/services/CommentService.js',
		'app/controllers/StateCreateCtrl.js',
		'app/controllers/NotificationCtrl.js',
		'app/controllers/LoginController.js',
		'app/controllers/RegisterCtrl.js',
		'app/controllers/EditCtrl.js',
		'app/controllers/SuscriptionCtrl.js',
		'app/services/StateService.js',
		'app/services/HomeService.js',
		'app/services/AuthService.js',
		'app/services/NotificationService.js',
		'app/services/RegisterService.js',
		'app/directives/bootstrap.js'
	],
	'js/app.js')
	.styles(['dist/css/style.css', 'dist/css/toaster.css', 'dist/css/chosen.css'], 'css/app.css')
	.setPublicPath('C:/laragon/www/angular')
	.version()
	.browserSync({
    	proxy: 'http://angular.dev',
	    files: [
	        "app/templates/*.html",
	        "app/templates/partials/*.html",
	        "app/controllers/*.js",
	        "app/services/*.js",
	        "app/templates/*.js",
	        "app/*.js",
	        "css/*.css",
	        "*.php",
	        "*.html"
	    ],
    	notify: false
	})