(function() {
    'use strict';

angular.module('app')
    .controller('RegisterCtrl', RegisterCtrl);

RegisterCtrl.$inject = ['$state', 'API_URL', 'Auth', 'View', 'RegisterService', 'StateService', 'toaster'];

function RegisterCtrl($state, API_URL, Auth, View, RegisterService, StateService, toaster){

	View.title('Crear tu cuenta')

    var vm = this
    vm.form = {}
    vm.input = 'password'
    vm.countries = StateService.getCountries()

    vm.hideShowPassword = function(){
    if (vm.input == 'password')
        vm.input = 'text'
    else
        vm.input = 'password'
    }

    StateService.getCountry().then(function(response){
        response.data[1] !== undefined ?
            vm.form.country_id = response.data[1].id :
            vm.form.country_id = vm.countries[0].id
    })

    vm.register = function(){
        RegisterService.register(vm.form).then(function(response){

            Auth.setUser(response.data.user)
            Auth.setAuth()
            toaster.pop({ type: 'success', title: '¡OK!', body: 'Cuenta creada' })
            localStorage.satellizer_token = response.data.token
            $state.transitionTo('app.index', {}, { reload: true })

        }).catch(function(response){
            var errors = []

            angular.forEach(response.data.errors, function(value, key){
                errors += value[0] + '<br>'
            })

           toaster.pop({
                type: 'error',
                title: '¡Error!',
                bodyOutputType: 'trustedHtml',
                body: errors
            })
        })
    }

}

})();