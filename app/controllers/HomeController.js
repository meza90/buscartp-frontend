(function() {
    'use strict';

angular.module('app')
    .controller('HomeCtrl', HomeCtrl);

HomeCtrl.$inject = ['API_URL', '$http', 'ChannelsFactory', 'View'];

function HomeCtrl(API_URL, $http, ChannelsFactory, View){

    View.title('Canales y TPs de Amazonas, Star One C2 y Arsat 2')

    var vm = this

    vm.satelite = 2
    vm.satelites = [
        {'id' : 2, 'name' : '61W Amazonas'},
        {'id' : 4, 'name' : '70W Star One'},
        {'id' : 1, 'name' : '81W Arsat 2'}
    ]

    vm.loader = true
    vm.transponders = []
    vm.channels = []

    ChannelsFactory.get().then(function(response){
        vm.channels = response.data.channels
        vm.transponders = response.data.transponders
        vm.loader = false
    }, function(error){
        console.log(error)
    })

    vm.search = function(transponder, satelite){
        ChannelsFactory.get(transponder, satelite).then(function(response){
            vm.channels = response.data.channels
            vm.transponders = response.data.transponders
            vm.loader = false
        }, function(error){
            console.log(error)
        })
    }

    vm.clearTp = function(){

        vm.channels = []
        vm.satelite = 2;
        vm.transponder = ''
        vm.input = ''
        vm.loader = true

        ChannelsFactory.get().then(function(response){
            vm.channels = response.data.channels
            vm.transponders = response.data.transponders
            vm.loader = false
        });
    }

    vm.clearInput = function(){
        vm.input = ''
    }
}

})();