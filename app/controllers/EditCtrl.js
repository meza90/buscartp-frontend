(function() {
    'use strict';

angular.module('app')
    .controller('EditCtrl', EditCtrl);

EditCtrl.$inject = ['$state', 'API_URL', 'RegisterService', 'Auth', 'View', 'StateService', '$rootScope', 'toaster'];

function EditCtrl($state, API_URL, RegisterService, Auth, View, StateService, $rootScope, toaster){

	View.title('Editar cuenta')

    var vm = this
    vm.form = {
    	country_id: $rootScope.user.country_id,
    	name: $rootScope.user.name,
    	email: $rootScope.user.email,
        username: $rootScope.user.username,
        password: ''
    }

    vm.input = 'password'
    vm.countries = StateService.getCountries()

    vm.hideShowPassword = function(){
    if (vm.input == 'password')
        vm.input = 'text'
    else
        vm.input = 'password'
    }

    vm.edit = function(){
        RegisterService.edit(vm.form).then(function(response){
            Auth.setUser(response.data.user)
            $state.transitionTo($state.current, {}, {reload: true})
            toaster.pop({ type: 'success', title: 'OK!', body: 'Cuenta editada' })
        })
    }

}

})();