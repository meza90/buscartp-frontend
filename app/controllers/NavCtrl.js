(function() {
    'use strict';

angular.module('app')
    .controller('NavCtrl', NavCtrl);

NavCtrl.$inject = ['$localStorage', 'Auth', '$state', '$rootScope'];

function NavCtrl($localStorage, Auth, $state, $rootScope){

	var vm = this

    $rootScope.auth = Auth.getAuth()
    $rootScope.user = Auth.getUser()

    vm.logout = function(){
        Auth.logout()
        $state.transitionTo('app.index', {}, { reload: true })
    }
}

})();