(function() {
    'use strict';

angular.module('app')
    .controller('NotificationCtrl', NotificationCtrl);

NotificationCtrl.$inject = ['API_URL', '$http', 'ValidationService', 'NotificationService', 'View', '$rootScope', 'toaster'];

function NotificationCtrl(API_URL, $http, ValidationService, NotificationService,
                            View, $rootScope, toaster){

    View.title('Suscribete')

    var vm = this
    vm.models = []
    vm.form = {
        type_id: []
    }

    if($rootScope.auth){
        NotificationService.getModels().then(
            function(response){
                vm.models = response.data.models
                vm.form.type_id = response.data.selected
            }
        )
    }

    vm.notifyModels = function(){
    	NotificationService.notifyModels(vm.form).then(
    		function(response){
                toaster.pop({ type: 'success', title: '¡OK!', body: 'Modelos guardados' })
    		}
    	)
    }
}

})();