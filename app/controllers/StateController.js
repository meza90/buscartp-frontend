(function() {
    'use strict';

angular.module('app')
    .controller('StateCtrl', StateCtrl);

StateCtrl.$inject = ['API_URL', '$http', 'ValidationService', 'StateService', 'View'];

function StateCtrl(API_URL, $http, ValidationService, StateService, View){

    View.title('Estado decodificadores')

    var vm = this

    vm.marks = StateService.getMarks()
    vm.models = []
    vm.form = {}
    vm.page = 1
    vm.states = []
    vm.show = false
    vm.reset = false

    vm.nextPage = function(){

        vm.pause = true
        vm.loader = true

        StateService.states(vm.page++, vm.form.mark_id, vm.form.model_id, vm.form.method, vm.form.state)
            .then(function(response){

                var states = response.data.states.data

                if(response.data.states.to === response.data.states.total) {
                    vm.states = vm.states.concat(states)
                    vm.pause = true
                }
                else {
                    vm.pause = false
                    vm.states = vm.states.concat(states)
                    vm.total = response.data.states.total
                }

                vm.loader = false
                vm.reset = false
            })
    }

    vm.filter = function(mark){
        StateService.marks(mark).then(function(response){
            vm.models = response.data.models
        })
    }

    vm.formFilter = function(){
        StateService.states(1, vm.form.mark_id, vm.form.model_id, vm.form.method, vm.form.state)
            .then(function(response){

                vm.total = response.data.states.total
                vm.page = 2

                if(response.data.states.data.length){

                    vm.show = false
                    vm.states = response.data.states.data

                    if(response.data.states.to === vm.total)
                        vm.pause = true
                    else
                        vm.pause = false
                        vm.loader = false

                } else {
                    vm.show = true
                    vm.states = []
                }
            })
    }

    vm.clean = function(){
        vm.loader = true
        vm.form = {}
        vm.page = 1
        vm.pause = false
        vm.show = false
        vm.reset = true
        StateService.states(vm.page++).then(function(response){
            vm.states = response.data.states.data
            vm.total = response.data.states.total
            vm.loader = false
        })
    }
}

})();