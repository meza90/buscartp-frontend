(function() {
    'use strict';

angular.module('app')
    .controller('StateCreateCtrl', StateCreateCtrl);

StateCreateCtrl.$inject = ['API_URL', '$http', 'ValidationService', '$rootScope',
                            'StateService', '$state', 'View', 'toaster'];

function StateCreateCtrl(API_URL, $http, ValidationService, $rootScope,
                        StateService, $state, View, toaster){

    View.title('Ingresar nuevo estado')

	var vm = this

	vm.form = {}
	vm.models = []
    vm.countries = StateService.getCountries()

    vm.marks = [
        {"id":2,"name":"Azamerica"},
        {"id":9,"name":"AzFox"},
        {"id":8,"name":"Cinebox"},
        {"id":7,"name":"Freei"},
        {"id":3,"name":"Freesky"},
        {"id":1,"name":"Miuibox"},
        {"id":6,"name":"QMDA"},
        {"id":5,"name":"Tocomfree"},
        {"id":4,"name":"TocomSat"}
    ];

    vm.satelites = [
		{"id": 9, "name":"22W Ses 4"},
        {"id": 3, "name":"30W Hispasat"},
        {"id": 5, "name":"58W Intelsat"},
        {"id": 2, "name":"61W Amazonas"},
        {"id": 8, "name":"63W Telstar 14R"},
        {"id": 4, "name":"70W Star One"},
        {"id": 7, "name":"71.8W Arsat 1"},
        {"id": 1, "name":"81W Arsat 2"},
        {"id": 6, "name":"87.2W TKSAT"},
		{"id": 10, "name": "107.3W Anik G"}
	]

    if($rootScope.auth) vm.form.country_id = $rootScope.user.country_id
    else {
        StateService.getCountry().then(function(response){
            response.data[1] !== undefined ?
                vm.form.country_id = response.data[1].id :
                vm.form.country_id = vm.countries[0].id
        })
    }

    vm.filter = function(mark){
        StateService.marks(mark).then(function(response){
            vm.models = response.data.models
            vm.form.model_id = vm.models[0].id
        })
    }

    vm.send = function(){
    	StateService.newState(vm.form).then(function(){
            toaster.pop({ type: 'success', title: '¡OK!', body: 'Estado enviado' })
            $state.transitionTo('app.states')
    	}, function(error){
            alert(error.data.message)
        })
    }
}

})();