(function() {
    'use strict';

angular.module('app')
    .controller('CommentCtrl', CommentCtrl);

CommentCtrl.$inject = ['$http', 'CommentService', '$location', '$stateParams',
						'ValidationService', '$scope', 'View', 'toaster'];

function CommentCtrl($http, CommentService, $location, $stateParams,
					ValidationService, $scope, View, toaster){

	View.title("Detalle estado decodificador")

	var vm = this

	vm.id = $stateParams.id
	vm.comments = []
	vm.form = {}
	vm.exists = true

	CommentService.comments(vm.id).then(function(response){
		vm.comments = response.data.comments
		vm.state = response.data.state
		vm.exists = response.data.exists
	}, function(){
		$state.go('states')
	})

	vm.comment = function(){
		CommentService.addComment(vm.id, vm.form).then(function(response){
			var comment = { body: response.data.comment, created_human_at: response.data.created_human_at }
			vm.comments.unshift(comment)
			vm.form = {}
		    new ValidationService().resetForm( $scope.formComment )
		})
	}

	vm.suscribe = function(){
		CommentService.suscribe(vm.id).then(function(response){
			vm.exists = true
			toaster.pop({ type: 'success', title: '¡OK!', body: 'Suscrito correctamente.' })
		})
	}

}

})();