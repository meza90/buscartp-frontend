(function() {
    'use strict';

angular.module('app')
    .controller('TpCtrl', TpCtrl);

TpCtrl.$inject = ['$stateParams', 'ChannelsFactory', '$location', 'View'];

function TpCtrl($stateParams, ChannelsFactory, $location, View){

    var vm = this
    var satelite = $stateParams.satelite.replace(/\-/g,' ')

    View.title(`Listado de TPs de Amazonas ${satelite}`)

    vm.transponders = {}
    vm.count = ''
    vm.satelite = satelite
    vm.loader = true

    ChannelsFactory.getTps(vm.satelite).then(function(response){
        vm.transponders = response.data.transponders
        vm.count = response.data.count
        vm.loader = false
    }, function(error){
        $location.path('/')
    })
}

})();