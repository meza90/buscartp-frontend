(function() {
    'use strict';

angular.module('app')
    .controller('SuscriptionCtrl', SuscriptionCtrl);

SuscriptionCtrl.$inject = ['RegisterService', 'View', 'toaster', '$window'];

function SuscriptionCtrl(RegisterService, View, toaster, $window){

    View.title('Actividad de tu cuenta')

    var vm = this

    vm.suscriptions = []
    vm.states = []
    vm.comments = []
    vm.index = ''
    vm.data = {}

    RegisterService.suscriptions().then(function(response){
        vm.suscriptions = response.data.suscriptions
        vm.states = response.data.states
        vm.comments = response.data.comments
    })

    vm.delete = function() {
        vm.index = vm.data.id
        RegisterService.deleteState(vm.data.id).then(function(response){
            $('#modal-delete').modal('hide')
            toaster.pop({   type: 'success',
                            title: '¡OK!',
                            body: 'Has eliminado la suscripción'
                        })
            vm.suscriptions.splice(vm.data.index, 1)
        })
    }

    vm.modal = function(index, id){
        vm.data.index = index
        vm.data.id = id
    }

}

})();