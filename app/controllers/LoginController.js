(function() {
    'use strict';

angular.module('app')
    .controller('LoginCtrl', LoginCtrl);

LoginCtrl.$inject = ['$state', 'Auth', 'View', 'toaster'];

function LoginCtrl($state, Auth, View, toaster){

	View.title('Iniciar sesión')

    var vm = this
    vm.credentials = {}

    vm.login = function(){
        Auth.login(vm.credentials).then(function(response){
            Auth.setUser(response.data.user)
            Auth.setAuth()
            $state.transitionTo('app.index', {}, { reload: true })
        }, function(){
            toaster.pop({ type: 'error', title: '¡Error!', body: 'Datos incorrectos' })
        });
    }

}

})();