$(document).on('click','.navbar-collapse.in',function(e) {
    if( $(e.target).is('a') ) {
        $(this).collapse('hide')
    }
});

$(document).popover({
    selector: '[data-toggle="popover"]'
})

$(document).on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

$(window).on('resize load', function(){
	if($(window).width() > 750)
		$('.chosen-tp-filter').next().css('width', '250px')
	else
		$('.chosen-tp-filter').next().css('width', '100%')

    $('.chosen-select-filters').next().css('width', '100%')
});

angular.module("app",
[
	'ui.router',
	'angular-loading-bar',
	'ghiscoding.validation',
	'pascalprecht.translate',
	'satellizer',
	'infinite-scroll',
	'angular.chosen',
	'toaster',
	'ngStorage',
	'ngSanitize'
])

.filter('conax', function(){
	return function(input){
		return input.replace('Conax', '')
	}
})

.constant('API_URL', 'http://search.loc')

.service('View', ['$rootScope', function($rootScope){
    return {
        title: function(title){
            $rootScope.title = title;
        }
    }
}])

.run(['$rootScope', 'Auth', '$state', function ($rootScope, Auth, $state) {
	    $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
	        if(Auth.isAuth() && (toState.name === 'app.login' || toState.name === 'app.register')) {
	        	event.preventDefault()
	        	$state.transitionTo('app.index')
	        }

	        if(toState.authenticate && !Auth.isAuth())
	        {
	        	event.preventDefault()
	        	$state.transitionTo('app.login')
	        }
	});
}])

.factory('TokenIntecerptor', ['$q', '$location', 'toaster', '$localStorage', '$rootScope',  function ($q, $location, toaster, $localStorage, $rootScope) {
    return {
        'responseError': function(rejection) {
        	var rejectionReasons = [
        	 	'token_not_provided', 'token_expired', 'token_absent', 'token_invalid'
        	 ];

        	 angular.forEach(rejectionReasons, function(value, index){
	    	 	if(rejection.data.error === value || rejection.status === 400)
	    	 	{
	    	 		delete $rootScope.auth
	    	 		delete $rootScope.user
	    	 		delete $localStorage.auth
	    	 		delete $localStorage.user

	    	 		toaster.pop({
						type: 'error',
						title: '¡Error!',
						body: 'Vuelve a iniciar sesión'
					})

	    	 		$location.path('/')
	    	 	}
        	 })

            return $q.reject(rejection);
         }
     };
}])

.directive('autofocus', ['$timeout', function($timeout) {
  return {
    restrict: 'A',
    link : function($scope, $element) {
      $timeout(function() {
        $element[0].focus();
      });
    }
  }
}])

 .directive('loading', function ()
 {
     return {
		restrict: 'E',
		template: `
	        <h3 class="text-center">
	            <i class="fa fa-circle-o-notch fa-spin fa-5x fa-fw"></i>
	        </h3>
		`,
		scope: {
			check: "&"
		},
		link: function (scope, elm, attrs)
		{
			scope.$watch(scope.check, function (v)
			{
				if(v){
					elm.show();
				}else{
					elm.hide();
				}
			})
		}
     };
 })