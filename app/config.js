(function() {
    'use strict';

angular
    .module('app')
    .config(config);

config.$inject = [
        '$stateProvider',
        '$translateProvider',
        '$urlRouterProvider',
        '$authProvider',
        'API_URL',
        '$locationProvider',
        '$httpProvider'
];

function config($stateProvider, $translateProvider, $urlRouterProvider, $authProvider, API_URL, $locationProvider, $httpProvider){

    $authProvider.loginUrl = API_URL + '/authenticate'
    $httpProvider.interceptors.push('TokenIntecerptor')
    $httpProvider.interceptors.push(['$q', '$rootScope', function($q, $rootScope) {
        var numberOfHttpRequests = 0;
        return {
            request: function (config) {
                numberOfHttpRequests += 1;
                $rootScope.waiting = true;
                return config;
            },
            requestError: function (error) {
                numberOfHttpRequests -= 1;
                $rootScope.waiting = (numberOfHttpRequests !== 0);
                return $q.reject(error);
            },
            response: function (response) {
                numberOfHttpRequests -= 1;
                $rootScope.waiting = (numberOfHttpRequests !== 0);
                return response;
            },
            responseError: function (error) {
                numberOfHttpRequests -= 1;
                $rootScope.waiting = (numberOfHttpRequests !== 0);
                return $q.reject(error);
            }
        };
    }]);

    $translateProvider.useStaticFilesLoader({
      files: [{
          prefix: 'js/validation/',
          suffix: '.json'
        }]
      });

    $translateProvider.preferredLanguage('es');
    $translateProvider.useSanitizeValueStrategy('escapeParameters');

    $stateProvider

      .state('app', {
        abstract: true,
        templateUrl: 'app/templates/partials/nav.html',
        controller: 'NavCtrl as main'
      })

      .state('app.index', {
          url: '/',
          templateUrl: 'app/templates/main.html',
          controller: 'HomeCtrl as home',
      })

      .state('app.transponders', {
          url: '/transponders/:satelite',
          templateUrl: 'app/templates/transponders.html',
          controller: 'TpCtrl as tp'
      })

      .state('app.states', {
          url: '/states',
          templateUrl: 'app/templates/states.html',
          controller: 'StateCtrl as state'
      })

      .state('app.create', {
          url: '/states/create',
          templateUrl: 'app/templates/state-create.html',
          controller: 'StateCreateCtrl as stateCreate'
        })

      .state('app.comments', {
          url: '/states/:id/comments',
          templateUrl: 'app/templates/comments.html',
          controller: 'CommentCtrl as comment'
      })

      .state('app.notificions', {
          url: '/notifications',
          templateUrl: 'app/templates/notifications.html',
          controller: 'NotificationCtrl as notification'
      })

      .state('app.register', {
          url: '/register',
          templateUrl: 'app/templates/register.html',
          controller: 'RegisterCtrl as register'
      })

      .state('app.login', {
          url: '/login',
          templateUrl: 'app/templates/login.html',
          controller: 'LoginCtrl as login'
      })

      .state('app.edit', {
          url: '/edit',
          templateUrl: 'app/templates/edit.html',
          controller: 'EditCtrl as edit',
          authenticate: true
      })

      .state('app.history', {
          url: '/history',
          templateUrl: 'app/templates/suscriptions.html',
          controller: 'SuscriptionCtrl as suscription',
          authenticate: true
      });

    $urlRouterProvider.otherwise('/')

    $locationProvider.html5Mode(true).hashPrefix('!')
}

})();