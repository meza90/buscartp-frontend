(function() {
    'use strict';

angular
    .module('app')
    .factory('StateService', StateService);

StateService.$inject = ['$http', 'API_URL', 'ValidationService', '$q'];

function StateService($http, API_URL, ValidationService, $q) {

    return {
        states: states,
        marks: marks,
        newState: newState,
        getMarks: getMarks,
        getCountry: getCountry,
        getCountries: getCountries
    };

    function states(page, mark_id, model_id, method, state) {

        var deferred = $q.defer();

        $http({
            url: `${API_URL}/`,
            params: { page : page, mark_id: mark_id, model_id: model_id, method: method, state: state }
        }).then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function marks(mark_id) {

        var deferred = $q.defer();

        $http({
            url: `${API_URL}/models/${mark_id}`
        }).then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function newState(form) {

        var deferred = $q.defer();

        $http.post(`${API_URL}/states`, form)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;

    }

    function getMarks() {
        return [
            {"id":2,"name":"Azamerica"},
            {"id":9,"name":"AzFox"},
            {"id":8,"name":"Cinebox"},
            {"id":7,"name":"Freei"},
            {"id":3,"name":"Freesky"},
            {"id":1,"name":"Miuibox"},
            {"id":6,"name":"QMDA"},
            {"id":5,"name":"Tocomfree"},
            {"id":4,"name":"TocomSat"}
        ];
    }

    function getCountry() {

        var deferred = $q.defer();

        $http({
            url: `${API_URL}/states/geo`,
            cache: true
        })
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function getCountries() {
        return [
            {'id': 1, 'name': "Chile"},
            {'id': 2, 'name': "Argentina"},
            {'id': 6, 'name': "Bolivia"},
            {'id': 10, 'name': "Brasil"},
            {'id': 4, 'name': "Colombia"},
            {'id': 3, 'name': "Ecuador"},
            {'id': 14, 'name': "El Salvador"},
            {'id': 16, 'name': "Estados Unidos"},
            {'id': 13, 'name': "Guatemala"},
            {'id': 11, 'name': "México"},
            {'id': 15, 'name': "Nicaragua"},
            {'id': 12, 'name': "Panamá"},
            {'id': 9, 'name': "Paraguay"},
            {'id': 5, 'name': "Perú"},
            {'id': 8, 'name': "Uruguay"},
            {'id': 7, 'name': "Venezuela"}
        ]
    }
}

})();