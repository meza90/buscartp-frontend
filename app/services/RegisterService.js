(function() {
    'use strict';

angular
    .module('app')
    .factory('RegisterService', RegisterService);

RegisterService.$inject = ['$http', 'API_URL', '$q'];

function RegisterService($http, API_URL, $q) {

    return {
        register: register,
        edit: edit,
        suscriptions: suscriptions,
        deleteState: deleteState
    };

    function register(form) {

        var deferred = $q.defer();

        $http.post(`${API_URL}/users`, form)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function notifyModels(form) {

        var deferred = $q.defer();

        $http.post(`${API_URL}/users/models`, form)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;

    }

    function edit(form) {
        var deferred = $q.defer();

        $http.put(`${API_URL}/users/edit`, form)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function suscriptions() {
        var deferred = $q.defer();

        $http.get(`${API_URL}/users/suscriptions`)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function deleteState(id) {
        var deferred = $q.defer();

        $http.delete(`${API_URL}/users/suscriptions/${id}`)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

}

})();