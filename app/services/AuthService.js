(function() {
    'use strict';

angular
    .module('app')
    .factory('Auth', Auth);

Auth.$inject = ['$auth', '$q', '$rootScope', '$location', '$localStorage'];

function Auth($auth, $q, $rootScope, $location, $localStorage) {

    return {
        login: login,
        isAuth: isAuth,
        setAuth: setAuth,
        getAuth: getAuth,
        setUser: setUser,
        getUser: getUser,
        logout: logout
    };

    function login(data) {

        var deferred = $q.defer();

    	$auth.login(data).then(function(data) {
            deferred.resolve(data);
    	}, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function isAuth() {
        return this.getAuth() ? true : false
    }

    function setAuth() {
        $localStorage.auth = true
    }

    function getAuth() {
        return $localStorage.auth
    }

    function setUser(user) {
        $localStorage.user = user
    }

    function getUser(){
        return $localStorage.user
    }

    function logout(){
        delete $rootScope.auth
        delete $localStorage.auth
        delete $rootScope.user
        delete $localStorage.user
        $auth.logout()
    }
}

})();