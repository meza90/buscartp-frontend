(function() {
    'use strict';

angular
    .module('app')
    .factory('NotificationService', NotificationService);

NotificationService.$inject = ['$http', 'API_URL', '$q'];

function NotificationService($http, API_URL, $q) {

    return {
        getModels: getModels,
        notifyModels: notifyModels
    };

    function getModels() {

        var deferred = $q.defer();

        $http.get(`${API_URL}/models`)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function notifyModels(form) {

        var deferred = $q.defer();

        $http.post(`${API_URL}/users/models`, form)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;

    }
}

})();