(function() {
    'use strict';

angular
    .module('app')
    .factory('CommentService', CommentService);

CommentService.$inject = ['$http', 'API_URL', '$q'];

function CommentService($http, API_URL, $q) {

    return {
        comments: comments,
        addComment: addComment,
        suscribe: suscribe
    };

    function comments(state) {

        var deferred = $q.defer();

        $http({
            url: `${API_URL}/states/${state}/comments`
        }).then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function addComment(state, form) {

        var deferred = $q.defer();

        $http.post(`${API_URL}/states/${state}/comments`, form)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }

    function suscribe(id) {
        var deferred = $q.defer();

        $http.post(`${API_URL}/suscribe/${id}/state`)
        .then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }
}

})();