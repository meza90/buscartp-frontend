(function() {
    'use strict';

angular
    .module('app')
    .factory('ChannelsFactory', ChannelsFactory);

ChannelsFactory.$inject = ['$http', 'API_URL', '$q'];

function ChannelsFactory($http, API_URL, $q) {

    return {
        get: get,
        getTps: getTps
    };

    function get(transponder, satelite) {

        var deferred = $q.defer();

        $http({
                url: `${API_URL}/channels`,
                method: 'GET',
                params: { transponder : transponder, satelite: satelite }
            }).then(function(data) {
                deferred.resolve(data);
            }, function(data){
                deferred.reject(data);
            });

        return deferred.promise;
    }

    function getTps(satelite) {

        var deferred = $q.defer();

        $http({
            url: `${API_URL}/transponders/${satelite}`,
            cache: true
        }).then(function(data) {
            deferred.resolve(data);
        }, function(data){
            deferred.reject(data);
        });

        return deferred.promise;
    }
}

})();