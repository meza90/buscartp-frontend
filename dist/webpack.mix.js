let mix = require('laravel-mix');

mix.js([
		'dist/js/chosen.js',
		// 'dist/js/angular-route.js',
		'dist/js/angular-translate.js',
		'dist/js/angular-chosen.js',
		'dist/js/angular-translate-loader-static-files.js',
		'dist/js/angular-validation.js',
		'dist/js/infinite-scroll.js',
		'dist/js/loading-bar.js',
		'dist/js/satellizer.js',
		// 'dist/js/angular-toastr.js',
		'dist/js/toaster.js'
	],
	'js/vendor.js')
	.setPublicPath('C:/laragon/www/angular')
	.version()